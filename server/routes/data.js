const express = require('express');
const router = express.Router();
const path = require('path');
/** This is an example of how to structure your api. This is using express.Router
 * see for more info.
 * @example
 * router.get('/', function (req, res) { ... }
 * @example
 * router.get('/:id', function (req, res) { ... }
 * @example
 * router.post('/', function (req, res) { ... }
 * */

// require the model (this is optional if you are using mongodb)
const Item = require("../../db/models/item.js");
router.get('/', function (req, res) {
  res.send('hello')
  // TODO: insert code to handle get requests
  // Item.find({}, function (error, doc) {
  //   // Log any errors
  //   if (error) {
  //     console.log(error);
  //   }
  //   // Or send the doc to the browser as a json object
  //   else {
  //     res.send(doc);
  //   }
  // });
});
router.post('/', function (req, res) {
  // TODO: insert code to handle post requests
  // const item = new Item(req.body);
  // item.save(function (error, doc) {
  //   // Send any errors to the browser
  //   if (error) {
  //     res.status(400).send(error);
  //   }
  //   // Otherwise, send the new doc to the browser
  //   else {
  //     res.status(200).send(doc);
  //   }
  // });
});
module.exports = router;
