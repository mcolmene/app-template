FROM node:8
WORKDIR /app-template
COPY . /app-template
COPY package.json /app-template
RUN npm install && npm run build
CMD npm start
EXPOSE 8080
