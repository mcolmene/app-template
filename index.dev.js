import React from 'react';
import { render } from 'react-dom';
import App from './src'
import 'babel-polyfill';

/** This is the entry point for the app DO NOT DELETE*/
render(
  <App />,
  document.getElementById('root')
);
