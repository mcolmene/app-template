import { combineReducers } from 'redux';
import users from './connected/reducer';

/** This is a sample reducer to show how combine reducer will combine
 * all reducers into one to be exported
 * @param {object} state - an object with state data
 * */
// Dummy reducer
function books(state = {}) {
  return state;
}
/** Combining reducers to export out to redux store*/
export default combineReducers({
  users,
  books,
});
