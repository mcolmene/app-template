import React, { Component } from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import User from './connected';
import '../../public/static/index.scss'
import image from '../../public/static/React-icon.svg';

const Compo = () => (
  <div>
    About us
  </div>
);
const Compa = () => (
  <div>
    Contact us
  </div>
);
const Main = () => (
  <main>
    <Switch>
      <Route exact path="/" component={User} />
      <Route path="/about" component={Compo} />
      <Route path="/contact" component={Compa} />
    </Switch>
  </main>
);
export default class Greeting extends Component {
  render() {
    return (
      <div>
        <header>
          <nav>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/about">About</Link></li>
              <li><Link to="/contact">Contact</Link></li>
            </ul>
          </nav>
        </header>
        <Main />
      </div>
    );
  }
}
