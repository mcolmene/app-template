import React from 'react';
import { connect }from 'react-redux';
import { updateUser } from './reducer';
/**
 * This is a sample component that has a onClick function that will be connected to the redux store
 * @param {function} updateUser - this is a function that will update the user in the redux store
 * */
const User = ({ updateUser }) => {
  return <div onClick={() => updateUser({ name: 'murphy', age: 5 })}>connected component</div>
}

const mapStateToProps = (state) => {
  return {
  name: state.users.name,
  age: state.users.age,
}};

const mapDispatchToProps = dispatch => ({
  updateUser: (userDetails) => dispatch(updateUser(userDetails))
})

export default connect(mapStateToProps, mapDispatchToProps)(User);
