export function updateUser(updatedDetails) {
  return {
    type: 'UPDATE_USER',
    userDetails: {...updatedDetails}
  }
}

const UPDATE_USER = 'UPDATE_USER';

export default function users (state = { name: 'Martin', age: 26 }, {type, userDetails}) {
  switch (type) {
    case UPDATE_USER:
      return Object.assign({}, state, userDetails);
    default :
      return state
  }
}
