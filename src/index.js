import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store';
import Component from './components/index';
/** This is the main app component that will be exported out to be rendered */
const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Component />
    </BrowserRouter>
  </Provider>
);

export default App;
