const mongoose = require("mongoose");

// Create the Schema class
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, "Amount is Required"]
  }
});

const Item = mongoose.model("Item", ItemSchema, "placeholder for name of collection in db");

module.exports = Item;
