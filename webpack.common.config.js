const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

require('dotenv').config();

const config = env => {
  const devMode = env === 'dev';
  return {
    entry: {
      bundle: path.join(__dirname, 'index.dev.js'),
      vendor: ['react']
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: 'app',
        filename: 'index.html',
        template: 'admin.html',
        appMountId: 'root',
        mobile: true
      }),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: '[name].css',
        chunkFilename: '[id].css'
      })
    ],
    output: {
      path: path.join(__dirname, 'build'),
      filename: '[name].js',
      chunkFilename: '[name].js',
      publicPath: '/'
    },
    optimization: {
      runtimeChunk: false,
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
        },
      }
    },
    module: {
      rules: [
        {
          test: /\.js|jsx$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
          }
        },
        {
          test: /\.s?[ac]ss$/,
          use: [
            {
              loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
            },
            {
              loader: 'css-loader',
              options: { minimize: !devMode }
            },
            {
              loader: 'sass-loader'
            },
          ],
        },
        {
          test: /\.json$/,
          use: {
            loader: 'json'
          }
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                publicPath: '/static',
                outputPath: 'static/'
              }
            },
            {
              loader: 'image-webpack-loader',
              options: {
                mozjpeg: {
                  progressive: true,
                  quality: 65
                },
                // optipng.enabled: false will disable optipng
                optipng: {
                  enabled: false,
                },
                pngquant: {
                  quality: '65-90',
                  speed: 4
                },
                gifsicle: {
                  interlaced: false,
                },
                // the webp option will enable WEBP
                webp: {
                  quality: 75
                }
              }
            },
          ],
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx', '.scss']
    }
  }
};
module.exports = {
  env: process.env,
  config
};
