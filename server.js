const express = require('express');
// logging for the application
const logger = require('morgan');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 8080;
// include environment variables
require('dotenv').config();

const db = mongoose.connection;

mongoose.Promise = Promise;

// location of API code
const dataApi = require('./server/routes/data.js');

// Configure our app for morgan and body parser
app.use(logger('dev'));

// parser for requests from clients
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Server will serve this route by default
app.use(express.static('build'));

// include api routes
app.use('/api/v1/data', dataApi);

// Used for syncing routes with the the front end
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
// connection to mlab
mongoose.connect(`mongodb://${process.env.USERNAME}:${process.env.PASSWORD}@${process.env.DB_URL}`);

// Log any mongoose errors
db.on('error', error => {
  console.log('Mongoose Error: ', error);
});

// Log a success message when we connect to our mongoDB collection with no issues
db.once('open', () => {
  console.log('Mongoose connection successful.');
});
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});
