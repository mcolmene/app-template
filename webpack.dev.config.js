const merge = require('webpack-merge');
const path = require('path');
const webpack = require('webpack');
const common = require('./webpack.common.config.js');

module.exports = env => {
  return merge(common.config(env), {
    mode: 'development',
    devtool: 'eval-source-map',
    plugins: [
      new webpack.DefinePlugin({
      })
    ],
    devServer: {
      contentBase: path.join(__dirname, "build"),
      compress: true,
      port: 3000,
      overlay: {
        warnings: true,
        errors: true
      },
      historyApiFallback: true,
      proxy: {
        "/api": "http://localhost:8080" // api is running on a different port
      }
    }
  })
};
