# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Boilerplate for creating a new application
* Version 1.0.0

### What is included? ###

* Jest test is set up
* linter with airbnb rules
* client side running on the server
* simple api configuration
* private env variables
* es6 capable
* React specific
* css or sass capable for styling
* production and development webpack configurations

### Package.json scripts ###

* dev - this will run both the server and webpack watch. NOTE: in order to use this shortcut ttab must be installed
globally as well as your IDE must have accessibility access to open new terminals.
* start - start the node server
* start:server - alternative to running the server if not using 'dev' command
* start:dev - will start webpack dev server with hot reload for better developer productivity
* prebuild - this is created to ensure test cases are meeting threshold and linting issues have been resolved
* webpack - alternative to running webpack for the project if not using 'dev' command
* build - will call 'build:clean' and create the production bundle.js for deployment
* build:clean - will clean the build folder
* deploy - will create bundle for production, create a commit with a default message and push it to your repo
* start - used by the server when deployed
* test - used to run Jest tests
* lint - used to run linter
* dep:update - update all of the out of date dependencies
* dep:list - list dependencies in the project
* doc - create documentation of the project
